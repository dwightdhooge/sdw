package setup;

import reporting.MobileTestReporter;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidKeyCode;
import org.junit.Assert;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;

import java.io.IOException;


public class MobileBasePage extends AbstractBasePage<AppiumDriver, MobileTestReporter> {
    public static final String NATIVE_CONTEXT = "NATIVE_APP";

    @Override
    public MobileTestReporter createTestReporter() {
        return MobileTestReporter.getInstance();
    }

    public void switchContext(final String contextName) {
        this.driver.context(contextName);
    }


    public void launchApp() {
        driver.launchApp();
    }

    public void closeApp() {
        driver.closeApp();
    }

    public void resetApp() {
        driver.resetApp();
    }

    //TODO CHange swipe methods so they work on both platforms

    public void swipeDown() {
        final Dimension size = this.driver.manage().window().getSize();
        final int startx = size.height / 2;
        final int starty = (int) (size.height * 0.6);
        final int endy = (int) (size.height * 0.2);

        TouchAction action = new TouchAction(driver);
        action.press(startx, starty).waitAction().moveTo(startx, endy).release().perform();

        testReporter.pass("User swiped down.");
    }

    public void swipeUp() {
        final Dimension size = this.driver.manage().window().getSize();
        final int startx = size.height / 2;
        final int starty = (int) (size.height * 0.2);
        final int endy = (int) (size.height * 0.9);

        TouchAction action = new TouchAction(driver);
        action.press(startx, starty).waitAction().moveTo(startx, endy).release().perform();

        testReporter.pass("User swiped up.");
    }

    public void swipeRight() {
        final Dimension size = this.driver.manage().window().getSize();
        final int startx = (int) (size.width * 0.9);
        final int endx = (int) (size.width * 0.20);
        final int starty = size.height / 2;

        TouchAction action = new TouchAction((MobileDriver) driver);
        action.press(startx, starty).waitAction().moveTo(endx, starty).release().perform();

        testReporter.pass("User swiped to the right.");
    }

    public void swipeLeft() {
        final Dimension size = this.driver.manage().window().getSize();
        final int startx = (int) (size.width * 0.8);
        final int endx = (int) (size.width * 0.1);
        final int starty = size.height / 2;

        TouchAction action = new TouchAction(driver);
        action.press(startx, starty).waitAction().moveTo(endx, starty).release().perform();

        testReporter.pass("User swiped to the left.");
    }

    // verify text of alert
    public void verifyAlert(final String ErrorMessage) {
        final String text = this.driver.findElementById("android:id/message").getText();
        Assert.assertEquals(text, ErrorMessage);
        System.out.print(text);
    }

    public void acceptAlert() {
        //ToDO add iOS part

        this.driver.findElementById("android:id/button1").click();
    }

    public void acceptPermission() {
        //ToDO add iOS part
        this.driver.findElementById("com.android.packageinstaller:id/permission_allow_button").click();
    }

    public void acceptAlert2() {
        //ToDO check difference with acceptAlert()
        final Alert alert = this.driver.switchTo().alert();
        alert.accept();
    }

    public void doubleClick(final By element) {
        WebElement elem = driver.findElement(element);
        TouchAction action = new TouchAction(driver);
        action.press(elem).release().perform().press(elem).release().perform();
    }

    public void longPress(final By element) {
        TouchAction action = new TouchAction(driver);
        action.longPress(findElement(element)).perform();
    }

    public void tapAndroidHomeButton() {
        ((AndroidDriver) driver).pressKeyCode(AndroidKeyCode.HOME);
    }

    public void back() {
        this.driver.navigate().back();
    }

    public void dismissKeyboard() {
        this.driver.hideKeyboard();
    }

    //TODO Check if wifi methods are possible on iOS
    public void disableWifi() {
        try {
            Runtime.getRuntime().exec("adb shell am broadcast -a io.appium.settings.wifi --es setstatus disable");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void enableWifi() {
        try {
            Runtime.getRuntime().exec("adb shell am broadcast -a io.appium.settings.wifi --es setstatus enable");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
        Verify Url of Mobile Webpage opened in external Browser
    */
    public void verifyOpenedUrl(final String link)
    {
        String source = driver.getPageSource();
        Assert.assertTrue("Url is not correct", source.contains(link));
        back();
    }
}
