package setup;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.reporter.configuration.Theme;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

// class for creating reporting
public class ExtentManager
{
    private static ExtentReports extent;

    public static ExtentReports getInstance()
    {
        if (extent == null)
            createInstance("test-output/reports/Automation Report " + getTimestamp() + ".html");

        return extent;
    }

    //Method to create a report Version
    public static ExtentReports createInstance(final String fileName)
    {
        final ExtentHtmlReporter htmlReporter = new ExtentHtmlReporter(fileName);
        htmlReporter.config().setTestViewChartLocation(ChartLocation.TOP);
        htmlReporter.config().setChartVisibilityOnOpen(true);
        htmlReporter.config().setTheme(Theme.STANDARD);
        htmlReporter.config().setDocumentTitle("Automation Report");
        htmlReporter.config().setEncoding("utf-8");
        htmlReporter.config().setReportName("Report " + getTimestamp());

        extent = new ExtentReports();
        extent.attachReporter(htmlReporter);

        htmlReporter.setAppendExisting(true);

        return extent;
    }

    // Method to get current Timestamp
    public static String getTimestamp()
    {
        final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        final LocalDateTime dateTime = LocalDateTime.now();

        final String timestamp = formatter.format(dateTime);

        return timestamp;
    }
}
