package setup;

import model.Constants;
import org.testng.TestNG;
import org.testng.xml.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TestStarter {
    private static final String TESTS_PACKAGE = "Activation";

    public static void main(String[] args) {
        final List<String> connectedDevices = getConnectedAndroidDevices();
        runTestNg(connectedDevices);
    }

    private static List<String> getConnectedAndroidDevices() {
        try {
            final Process process = Runtime.getRuntime().exec("adb devices");
            final BufferedReader in = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String line;

            final Pattern pattern = Pattern.compile("^([a-zA-Z0-9\\-]+)(\\s+)(device)");
            Matcher matcher;

            final List<String> connectedDevices = new ArrayList<>();

            while ((line = in.readLine()) != null) {
                if (line.matches(pattern.pattern())) {
                    matcher = pattern.matcher(line);
                    if (matcher.find()) {
                        connectedDevices.add(matcher.group(1));
                    }
                }
            }
            return connectedDevices;
        } catch (IOException e) {
            e.printStackTrace();
            return Collections.emptyList();
        }
    }


    /**
     * Creates a test suite and runs the project's tests on the supplied devices.
     */
    private static void runTestNg(List<String> devices) {
        final XmlSuite suite = createTestSuite();
        List<XmlSuite> suites = new ArrayList<>();
        suites.add(suite);

        for (String deviceId : devices) {
            createTest(suite, deviceId);
        }

        final TestNG tng = new TestNG();
        tng.setXmlSuites(suites);
        tng.setUseDefaultListeners(false);
        tng.run();
    }

    private static XmlSuite createTestSuite() {
        final XmlSuite suite = new XmlSuite();
        suite.setName("TmpSuite");
        suite.setParallel(XmlSuite.ParallelMode.TESTS);
        suite.setThreadCount(4);

        //suite.addListener("framework.Priority.PriorityInterceptor");

        return suite;
    }

    private static void createTest(final XmlSuite suite, final String deviceId) {
        XmlTest test = new XmlTest(suite);
        test.setName("TmpTest" + deviceId);

        Map<String, String> parameters = new HashMap<>();
//        parameters.put("deviceId", deviceId);
////        parameters.put("deviceName", getDeviceName(deviceId));
////        parameters.put("platformVersion", getAndroidVersion(deviceId));
////        parameters.put("platformName", "Android");
        parameters.put(Constants.DEVICE_ID, deviceId);
        parameters.put(Constants.DEVICE_NAME, getDeviceName(deviceId));
        parameters.put(Constants.PLATFORM_VERSION, getAndroidVersion(deviceId));
        parameters.put(Constants.PLATFORM_NAME, "Android");

       test.setParameters(parameters);
//
//        List<XmlClass> myClasses = new ArrayList<XmlClass>();
//        myClasses.add(new XmlClass("scenarios.Activation"));
//
//        test.setXmlClasses(myClasses);

        addGroups(test);

        addClasses(test);
    }

    private static void addClasses(XmlTest test) {


        String testClass = System.getProperty("testClass1");
        System.out.println(testClass);
        if (testClass.equals("all")) {
            List<XmlPackage> packages = new ArrayList<XmlPackage>();
            packages.add(new XmlPackage(TESTS_PACKAGE));
            test.setPackages(packages);
            System.out.println("packages" + Arrays.toString(packages.toArray()));
        } else {
            List<XmlClass> myClasses = new ArrayList<XmlClass>();
            int i = 1;
            while (!testClass.isEmpty() && !testClass.equals("false") && !testClass.equals("0")) {
                System.out.println("check" + testClass);
                System.out.println("Intheloop");
                myClasses.add(new XmlClass(TESTS_PACKAGE + "." + testClass));
                i++;
                testClass = System.getProperty("testClass" + i);
                System.out.println("Testje " + i + testClass);
            }
            test.setXmlClasses(myClasses);
            System.out.println("classes:" + Arrays.toString(myClasses.toArray()));

        }

    }


    private static void addGroups(XmlTest test) {
        final String testGroup = System.getProperty("testGroup");
        if (!testGroup.equals("all")) {
            XmlGroups xmlGroups = createGroupIncluding(testGroup);
            test.setGroups(xmlGroups);
        }
    }

    private static XmlGroups createGroupIncluding(String testGroup) {
        final XmlGroups xmlGroups = new XmlGroups();
        final XmlRun xmlRun = new XmlRun();
        xmlRun.onInclude(testGroup);
        xmlGroups.setRun(xmlRun);
        return xmlGroups;
    }

    private static String getAndroidVersion(final String deviceId) {

        return runAndReturnTerminalCommand("adb -s " + deviceId + " shell getprop ro.build.version.release");

    }

    private static String getDeviceName(final String deviceId) {
        final String brand = runAndReturnTerminalCommand("adb -s " + deviceId + " shell getprop ro.product.brand");
        final String model = runAndReturnTerminalCommand("adb -s " + deviceId + " shell getprop ro.product.model");
        return brand + " " + model;
    }

    /**
     * Runs a single terminal command and returns the first line of its output.
     */
    private static String runAndReturnTerminalCommand(final String command) {
        try {
            final Process process = Runtime.getRuntime().exec(command);
            final BufferedReader in = new BufferedReader(new InputStreamReader(process.getInputStream()));

            return in.readLine();
        } catch (IOException e) {
            e.printStackTrace();
            return "Unknown";
        }
    }

    private static String getAndroidDeviceLanguage(final String deviceId) {
        String language = null;

        String line = runAndReturnTerminalCommand("adb -s" + deviceId + " shell getprop persist.sys.locale");

        //String qmkj = line.substring(0, 2);

        String pattern = "^.*?(?=-)";

        Pattern r = Pattern.compile(pattern);

        Matcher m = r.matcher(line);

        if (m.matches()) {
            language = m.group(0);
        }

        return language;


    }
}

