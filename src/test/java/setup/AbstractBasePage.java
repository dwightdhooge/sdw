package setup;

import com.aventstack.extentreports.MediaEntityBuilder;
import driver.DriverProvider;
import reporting.AbstractTestReporter;
import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.collections.Lists;
import utils.TestUtils;

import java.io.IOException;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsCollectionContaining.hasItem;


public abstract class AbstractBasePage<T extends WebDriver, S extends AbstractTestReporter<T>> {

    private final int MAX_WAITING_TIME_SECONDS = 10;

    protected T driver;
    protected S testReporter;

    public AbstractBasePage() {
        final DriverProvider<T> driverProvider = new DriverProvider<>();
        driver = driverProvider.getDriver();
        testReporter = createTestReporter();
    }

    protected abstract S createTestReporter();

    protected WebElement findElement(final By element) {
        return driver.findElement(element);
    }

    protected List<WebElement> findElements(final By element) {
        return driver.findElements(element);
    }

    public void clickOnElement(final By element) {
        this.waitForElementPresent(element);
        findElement(element).click();
        passTest("Click on element '" + element + "'.", true);
    }

    /**
     * Click mutiple times on the same element
     */
    public void multipleClickOnElement(final By element, int times) {
        this.waitForElementPresent(element);
        WebElement elem = findElement(element);

        for (int i = 0; i < times; i++) {
            elem.click();
        }
    }

    /**
     * Enter Text in an element
     */
    public void enterText(final By element, final String strText) {
        this.waitForElementPresent(element);
        findElement(element).sendKeys(strText);
        passTest("Enter text '" + strText + "' in element '" + element + "'.", true);
    }

    public void hitEnter(final By element) {
        this.waitForElementPresent(element);
        findElement(element).sendKeys(Keys.ENTER);
        passTest("Hit enter key on element '" + element + "'.", true);
    }

    public void clearTextField(final By element) {
        findElement(element).clear();
    }

    /**
     * Test if element is present on the screen
     */
    public void testElementPresent(final By element) {
        final String path;

        if (this.isElementPresent(element)) {
            passTest("Element '" + element + "' is present.", true);
        } else {
            failTest("Element '" + element + "' is not present.");
        }
    }

    public Boolean isElementPresent(final By element) {
        return findElements(element).size() != 0;
    }

    /**
     * Waits for the element to be present but not necessarily visible.
     */
    public void waitForElementPresent(final By element) {
        try {
            final WebDriverWait wait = new WebDriverWait(driver, MAX_WAITING_TIME_SECONDS);
            wait.until(ExpectedConditions.presenceOfElementLocated(element));
            passTest("Waiting for element '" + element + "' to be present.", false);
        } catch (final TimeoutException e) {
            failTest("Waiting for element '" + element + "' to be present.");
            e.printStackTrace();
        }
    }

    /**
     * Waits for the element to be present and visible.
     */
    public void waitForElementVisible(final By element) {
        try {
            final WebDriverWait wait = new WebDriverWait(driver, MAX_WAITING_TIME_SECONDS);
            wait.until(ExpectedConditions.visibilityOfElementLocated(element));
            passTest("Waiting for element '" + element + "' to be visible.", true);
        } catch (final TimeoutException e) {
            failTest("Waiting for element '" + element + "' to be visible.");
            e.printStackTrace();
        }
    }

    /**
     * Waits for the element to be invisible (but can still be present).
     */
    public void waitForElementInvisible(final By element) {
        try {
            final WebDriverWait wait = new WebDriverWait(driver, MAX_WAITING_TIME_SECONDS);
            wait.until(ExpectedConditions.invisibilityOfElementLocated(element));
            passTest("Waiting for element '" + element + "' to be invisible.", true);
        } catch (final TimeoutException e) {
            failTest("Waiting for element '" + element + "' to be invisible.");
        }
    }

    /**
     * Waits for the element to be not present.
     */
    public void waitForElementRemoved(final By element) {
        try {
            final WebDriverWait wait = new WebDriverWait(driver, MAX_WAITING_TIME_SECONDS);
            wait.until(absenceOfElementLocated(element));
            passTest("Waiting for element '" + element + "' to be removed.", true);
        } catch (final TimeoutException e) {
            failTest("Waiting for element '" + element + "' to be removed.");
        }
    }


    /**
     * Get value of a property of an Element
     */
    public String getTextValueElement(final By element) {
        return findElement(element).getText();
    }

    public String getPropertyElement(final By element, final String property) {
        return findElement(element).getAttribute(property);
    }


    public void verifyTextValueElement(final By element, final String strExpectedValue) {
        final String text = getTextValueElement(element);

        try {
            Assert.assertEquals(strExpectedValue, text);
            passTest("Actual text value of element '" + element + "' is '" + text + "' and equals expected value '" + strExpectedValue + "'.", false);
        } catch (final AssertionError e) {
            failTest("Actual text value of element '" + element + "' is '" + text + "' and doesn't equal expected value '" + strExpectedValue + "'.");
            e.printStackTrace();
        }
    }

    /**
     * Verify if property of an element equals a certain value
     */
    public void verifyPropertyElement(final By element, final String property, final String strExpectedValue) {
        final String text = getPropertyElement(element, property);

        try {
            Assert.assertEquals(strExpectedValue, text);
            passTest("Actual value of property '" + property + "' of element '" + element + "' is '" + text + "' and equals expected value '" + strExpectedValue + "'.", false);
        } catch (final AssertionError e) {
            failTest("Actual value of property '" + property + "' of element '" + element + "' is '" + text + "' and doesn't equal expected value '" + strExpectedValue + "'.");
            e.printStackTrace();
        }
    }

    /**
     * For elements with the same ID's, check if one of them equals a certain property value
     */
    public void verifyPropertyElementInList(final By element, String strExpectedValue, final String Property) {
        List<WebElement> idlist = driver.findElements(element);
        List<String> texts = Lists.newArrayList();

        for (int i = 0; i < idlist.size(); i++) {
            WebElement item = idlist.get(i);
            final String text = item.getAttribute("text");
            texts.add(text);
        }
        assertThat(texts, hasItem(strExpectedValue));

    }


    private void passTest(String message, Boolean takeScreenshot) {
        final String path;

        if (takeScreenshot && testReporter.screenshotsAllowed()) {
            path = takeScreenshot();

            try {
                testReporter.pass(message, MediaEntityBuilder.createScreenCaptureFromPath(path).build());
            } catch (IOException e) {
                testReporter.pass(message);
                e.printStackTrace();
            }
        } else {
            testReporter.pass(message);
        }
    }

    private void failTest(String message) {
        final String path = takeScreenshot();

        try {
            testReporter.fail(message, MediaEntityBuilder.createScreenCaptureFromPath(path).build());
        } catch (IOException e) {
            testReporter.fail(message);
            e.printStackTrace();
        }
    }

    public void saveScreenshot() {
        final String path = takeScreenshot();

        try {
            testReporter.pass("Save screenshot of current state.", MediaEntityBuilder.createScreenCaptureFromPath(path).build());
        } catch (final IOException e) {
            e.printStackTrace();
        }
    }

    private String takeScreenshot() {
        return TestUtils.takeScreenshot((TakesScreenshot) driver);
    }

    private static ExpectedCondition<Boolean> absenceOfElementLocated(final By locator) {
        return new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver driver) {
                try {
                    driver.findElement(locator);
                    return false;
                } catch (NoSuchElementException e) {
                    return true;
                } catch (StaleElementReferenceException e) {
                    return true;
                }
            }

            @Override
            public String toString() {
                return "element to not being present: " + locator;
            }
        };
    }

}
