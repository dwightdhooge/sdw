package setup;


import driver.DriverProvider;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;
import model.Constants;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import reporting.MobileTestReporter;

import java.net.URL;
import java.util.concurrent.TimeUnit;



public class Setup implements ITestListener
{

    public AppiumDriver driver;
    public String appiumServiceUrl;
    private MobileTestReporter testReporter;
    //public Integer wdaDevicePort;

    public MobileTestReporter getTestReporter() {
        return testReporter;
    }

    protected void quitDriver(){driver.quit();}

    private static final String DRIVER = "driver";


    //public Eyes eyes = new Eyes();

    //Start Appium server programmatically on a random free port
    public void startAppium()
    {
        final AppiumDriverLocalService service = AppiumDriverLocalService.buildService(new AppiumServiceBuilder().usingAnyFreePort());

        service.start();

        this.appiumServiceUrl = service.getUrl().toString();



        //Random randomObj = new Random();
        // Create randomnumber to use as WDALocalPort
        //wdaDevicePort = randomObj.ints(8000, 9000).findFirst().getAsInt();
    }


    public void driverInitiation(final String device_id, final String device_name, final String platformVersion, final String platformName) throws Exception
    {
        final DesiredCapabilities capabilities = new DesiredCapabilities();


        capabilities.setCapability(MobileCapabilityType.APPIUM_VERSION, "1.6.5");
        capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, platformName);
        capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, platformVersion);
        capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, device_name);
        capabilities.setCapability(MobileCapabilityType.UDID, device_id);
        capabilities.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY, "com.sdworx.simon.SplashScreen");
        capabilities.setCapability(AndroidMobileCapabilityType.APP_PACKAGE, "com.sdworx.simon");


        capabilities.setCapability("app", System.getProperty("user.dir") + "/src/test/resources/app/" + "app.apk");

        //capabilities.setCapability("bundleId", "mobi.inthepocket.bcmc");
        //capabilities.setCapability(IOSMobileCapabilityType.LAUNCH_TIMEOUT, 500000);
        capabilities.setCapability(MobileCapabilityType.NO_RESET, "true");
        //capabilities.setCapability("wdaLocalPort", wdaDevicePort);
        //capabilities.setCapability("useNewWDA", true);


        this.driver = new AndroidDriver(new URL(this.appiumServiceUrl), capabilities);

        this.driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    // Method executed before every <Test> in TestNG file
    public void onStart(final ITestContext context)
    {
        final String deviceId = context.getCurrentXmlTest().getParameter("deviceId");
        final String deviceName = context.getCurrentXmlTest().getParameter("deviceName");
        final String platformVersion = context.getCurrentXmlTest().getParameter("platformVersion");
        final String platformName = context.getCurrentXmlTest().getParameter("platformName");

        //Start appium server
        startAppium();

          //create a parentNode per Device in Report
        testReporter = MobileTestReporter.init(driver, deviceId, deviceName, platformVersion, platformName);
        testReporter.onStart();


        //eyes.setApiKey("LILwUCBbUtBGlNx0kNs9K7OvL107nlS111vDGGfw3CSReGM110");



        try
        {
            driverInitiation(deviceId, deviceName, platformVersion, platformName);
        }
        catch (final Exception e)
        {
            e.printStackTrace();
        }

        context.setAttribute(DriverProvider.KEY_DRIVER, this.driver);
        context.setAttribute(Constants.PLATFORM_NAME, platformName);
    }

    // Method executed after every <Test> in TestNG file
    public void onFinish(final ITestContext context)
    {
        getTestReporter().onFinish(context);
        quitDriver();
        context.setAttribute(DriverProvider.KEY_DRIVER, null);
        //eyes.abortIfNotClosed();
    }


    // Method executed before every @Test method
    public void onTestStart(final ITestResult result) {
        getTestReporter().onTestStart(result);

    }

    // Method executed when @Test succeeds
    public void onTestSuccess(final ITestResult result) {
        getTestReporter().onTestSuccess(result);
    }

    // Method executed when @Test fails
    public void onTestFailure(final ITestResult result) {
       // getTestReporter().onTestFailure(result);
    }

    public void onTestSkipped(final ITestResult result) {
        getTestReporter().onTestSkipped(result);
    }


    public void onTestFailedButWithinSuccessPercentage(final ITestResult result) {
        getTestReporter().onTestFailedButWithinSuccessPercentage(result);
    }
}
