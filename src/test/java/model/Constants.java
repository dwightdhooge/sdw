package model;

public class Constants {

    public static final String DEVICE_ID = "deviceId";
    public static final String DEVICE_NAME = "deviceName";
    public static final String PLATFORM_VERSION = "platformVersion";
    public static final String PLATFORM_NAME = "platformName";
    public static final String BUILD_NUMBER = "buildNumber";
    public static final String DEVICE_LANGUAGE = "deviceLanguage";
}
