package model;


/**
 * Basic information of the device a test is running on.
 */
public class TestDevice {
    private Platform platform;
    private String deviceId;
    private String buildNumber;


    public TestDevice(Platform platform, String deviceId) {
        this.platform = platform;
        this.deviceId = deviceId;
    }

    public Platform getPlatform() {
        return platform;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public String getBuildNumber() {
        return buildNumber;
    }

    public void setBuildNumber(String buildNumber) {
        this.buildNumber = buildNumber;
    }

}
