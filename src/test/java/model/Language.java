package model;

import javax.annotation.Nonnull;

public enum Language {
    NL,
    FR,
    EN,
    DE,
    UNKNOWN;

    public static Language getLanguageFromDeviceLanguage(@Nonnull final String deviceLanguage) {
        for (Language language : values()) {
            if (language.name().equalsIgnoreCase(deviceLanguage)) {
                return language;
            }
        }
        return Language.UNKNOWN;
    }
}
