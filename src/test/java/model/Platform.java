package model;

import javax.annotation.Nonnull;

/**
 * The platform / operating system tests run on.
 */
public enum Platform {

    ANDROID("Android", ".apk"),
    IOS("iOS", ".ipa");

    private final String name;
    private final String fileExtension;

    Platform(@Nonnull final String name, @Nonnull final String fileExtension) {
        this.name = name;
        this.fileExtension = fileExtension;
    }

    public String getName() {
        return name;
    }

    public String getFileExtension() {
        return fileExtension;
    }

    public static Platform getPlatformFromName(@Nonnull final String name) {
        for (Platform platform : values()) {
            if (platform.getName().equalsIgnoreCase(name)) {
                return platform;
            }
        }
        return null;
    }
}
