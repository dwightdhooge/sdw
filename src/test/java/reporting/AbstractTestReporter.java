package reporting;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.MediaEntityModelProvider;
import org.testng.ITestContext;
import org.testng.ITestResult;
import setup.ExtentManager;
import java.io.IOException;

/**
 * Creates a report with the results of all tests.
 */
public abstract class AbstractTestReporter<T> {
    private static ExtentReports extent = ExtentManager.getInstance();
    private static ThreadLocal<ExtentTest> parentTest = new ThreadLocal<>();
    private static ThreadLocal<ExtentTest> test = new ThreadLocal<>();
    private T driver;

    AbstractTestReporter(final T driver) {
        this.driver = driver;
    }

    public synchronized void onStart() {
        final ExtentTest parent = extent.createTest(getTestReportTitle(), getTestReportDescription());
        parentTest.set(parent);
    }

    public synchronized void onFinish(final ITestContext context) {
        extent.flush();
    }

    public synchronized void onTestStart(final ITestResult result) {
        final ExtentTest child = parentTest.get().createNode(result.getMethod().getMethodName(), result.getMethod().getDescription());
        test.set(child);
    }

    public synchronized void onTestSuccess(final ITestResult result) {
        pass("Test finished.");
    }

    public synchronized void onTestFailure(final ITestResult result) {
        test.get().fail(result.getThrowable());
        final String path = takeScreenshot(driver);

        try {
            test.get().fail(result.getThrowable(), MediaEntityBuilder.createScreenCaptureFromPath(path).build());
        } catch (final IOException e) {
            e.printStackTrace();
        }
    }

    protected abstract String getTestReportTitle();

    protected abstract String getTestReportDescription();

    protected abstract String takeScreenshot(T driver);

    public synchronized void onTestSkipped(final ITestResult result) {
        test.get().skip(result.getThrowable());
    }

    public synchronized void onTestFailedButWithinSuccessPercentage(final ITestResult result) {
    }

    public void pass(final String details) {
        test.get().pass(details);
    }

    public void pass(final String details, final MediaEntityModelProvider provider) {
        test.get().pass(details, provider);
    }

    public void fail(final String details) {
        test.get().fail(details);
    }

    public void fail(final String details, final MediaEntityModelProvider provider) {
        test.get().fail(details, provider);
    }

    public Boolean screenshotsAllowed() {
        final String screenshotsAllowed = System.getProperty("screenshotsAllowed");
        return !(screenshotsAllowed != null && (screenshotsAllowed.equals("0") || screenshotsAllowed.equals("false")));
    }
}
