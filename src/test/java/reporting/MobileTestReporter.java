package reporting;

import io.appium.java_client.AppiumDriver;
import utils.TestUtils;

public final class MobileTestReporter extends AbstractTestReporter<AppiumDriver> {

    private static MobileTestReporter testReporter;

    private String deviceId;
    private String deviceName;
    private String platformVersion;
    private String platformName;
    private String buildNumber;

    private MobileTestReporter(final AppiumDriver driver, final String deviceId, final String deviceName, final String platformVersion, final String platformName) {
        super(driver);

        this.deviceId = deviceId;
        this.deviceName = deviceName;
        this.platformVersion = platformVersion;
        this.platformName = platformName;
        //this.buildNumber = buildNumber;
    }

    public static MobileTestReporter init(final AppiumDriver driver, final String deviceId, final String deviceName, final String platformVersion, final String platformName) {
        testReporter = new MobileTestReporter(driver, deviceId, deviceName, platformVersion, platformName);
        return testReporter;
    }

    public static MobileTestReporter getInstance() {
        if (testReporter == null) {
            throw new NullPointerException("You must first init this TestReporter with the init() method!");
        }

        return testReporter;
    }

    @Override
    protected String getTestReportTitle() {
        return "Build " + " on " + deviceName + " / " + platformName + " " + platformVersion;
    }

    @Override
    protected String getTestReportDescription() {
        return "Test on device with id: " + deviceId;
    }

    @Override
    protected String takeScreenshot(AppiumDriver driver) {
        return TestUtils.takeScreenshot(driver);
    }
}