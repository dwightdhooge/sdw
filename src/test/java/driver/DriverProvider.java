package driver;

import org.testng.ITestResult;
import org.testng.Reporter;

/**
 * Provides access to the driver of the currently running test.
 */
public class DriverProvider<T> {
    public static final String KEY_DRIVER = "driver";

    /**
     * @return The driver of the currently running test.
     */
    public T getDriver() {
        final ITestResult result = Reporter.getCurrentTestResult();

        if (result == null) {
            throw new UnsupportedOperationException("Please invoke only from within an @Test method");
        }

        final Object driver = result.getTestContext().getAttribute(KEY_DRIVER);

        if (driver == null) {
            throw new IllegalStateException("Unable to find a valid webdriver instance");
        }

        return (T) driver;
    }
}
