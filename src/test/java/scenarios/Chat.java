package scenarios;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import pages.ChatPage;
import pages.HomePage;
import pages.absencePages.AbsenceRequestPage;
import pages.menuPages.MenuPage;
import priority.Priority;
import setup.Setup;

@Priority(1)
@Listeners(Setup.class)
public class Chat {
    private HomePage homePage;
    private MenuPage menuPage;
    private ChatPage chatPage;
    private AbsenceRequestPage absenceRequestPage;

    @BeforeMethod(alwaysRun = true)
    public void setup() {
        homePage = new HomePage();
        menuPage = new MenuPage();
        chatPage = new ChatPage();

    }

    @Priority(2)
    @Test(description = "Test Chat", groups = "Test")
    public void Chat() {



        chatPage.openChat();
        chatPage.verifyMessageFromChat("How can I help you further?");
        chatPage.sendChatMessage("Dit is een test");
        chatPage.checkLoadingIndicator();
        //chatPage.waitLoadingIndicator();
        chatPage.verifyMessageFromUser("Dit is een test");
        chatPage.closeChat();
        //chatPage.sendChatAndCheck("Dit is een test");
        chatPage.openChat();
    }

}

