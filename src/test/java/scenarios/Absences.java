package scenarios;

import priority.*;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import pages.ChatPage;
import pages.HomePage;
import pages.absencePages.AbsenceOverviewPage;
import pages.absencePages.AbsenceRequestPage;
import pages.absencePages.ConfirmationRequestPage;
import pages.menuPages.MenuPage;
import setup.Setup;


@Priority(2)
@Listeners(Setup.class)
public class Absences {
    private HomePage homePage;
    private MenuPage menuPage;
    private AbsenceRequestPage absenceRequestPage;
    private AbsenceOverviewPage absenceOverviewPage;
    private ConfirmationRequestPage confirmationRequestPage;
    private ChatPage chatPage;

    @BeforeMethod(alwaysRun = true)
    public void setup() {

        homePage = new HomePage();
        menuPage = new MenuPage();
        absenceRequestPage = new AbsenceRequestPage();
        absenceOverviewPage = new AbsenceOverviewPage();
        confirmationRequestPage = new ConfirmationRequestPage();
        chatPage = new ChatPage();
    }

    @Priority(1)
    @Test(description = "Test Multiple Day Absence Request flow", groups = "Test")
    public void MultipleDaysAbsence() {
        homePage.openMenu();
        menuPage.TapAbsenceItem();
        absenceOverviewPage.addAbsenceRequest();
        absenceRequestPage.selectHolidayType();
        absenceRequestPage.selectMultipleDays();
        absenceRequestPage.selectStartDate("2", "Jan", true);
        absenceRequestPage.selectMultipleDay_StartDateAfternoon();
        absenceRequestPage.selectEndDate("4", "Jan", true);
        absenceRequestPage.selectMultipleDay_EndDateForenoon();
        absenceRequestPage.enterComment("This a Test for Multiple Days Holiday");
        absenceRequestPage.confirmAbsenceRequest();
    }

    @Priority(2)
    @Test(description = "Test Single Day Absence Request Flow", groups = "Regression")
    public void SingleDayAbsence() {
        homePage.openMenu();
        menuPage.TapAbsenceItem();
        absenceOverviewPage.waitForRemaingDays();
        final double startDays = absenceOverviewPage.getRemainingDays();
        absenceOverviewPage.addAbsenceRequest();
        absenceRequestPage.selectHolidayType();
        absenceRequestPage.selectOneDay();
        absenceRequestPage.selectStartDate("19", "Apr", false);
        absenceRequestPage.selectSingleDay_Forenoon();
        absenceRequestPage.enterComment("This is a Test for Single Day Holiday");
        absenceRequestPage.confirmAbsenceRequest();
        chatPage.closeChat();
        confirmationRequestPage.confirmAbsence();
        absenceOverviewPage.waitForRemaingDays();
        final double endDays = absenceOverviewPage.getRemainingDays();
        absenceOverviewPage.checkDateDifference(startDays, endDays, 0.5);
        absenceOverviewPage.navigateToPendingTab();
    }
}
