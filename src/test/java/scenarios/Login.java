package scenarios;

import priority.*;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import pages.HomePage;
import pages.loginPages.LoginPage;
import pages.loginPages.WelcomePage;
import setup.Setup;

@Priority(3)
@Listeners(Setup.class)
public class Login
{

    private HomePage homePage;
    private LoginPage loginPage;
    private WelcomePage welcomePage;

    @BeforeMethod(alwaysRun = true)
    public void setup()
    {

        homePage = new HomePage();
        loginPage = new LoginPage();
        welcomePage = new WelcomePage();
    }

    @Priority(6)
    @Test(description = "Verify if FAQ can be opened from Welcome Screen", groups = "Test")
    public void FAQ()
    {

        welcomePage.openFaq();

        //welcomePage.verifyFAQ("https://www.myworkandme.com/portalmwam/faq");

        welcomePage.back();
    }

    //@priority(4)
    @Test(description = "verify cancel button on Login Page", groups = "Regression")
    public void CancelLoginFlow()
    {
        welcomePage.tapLoginButton();

        loginPage.cancelLogin();

        welcomePage.verifyPage();
    }

    //@Test(description = "verify false login flow")
    public void FalseLoginFlow()
    {
        welcomePage.tapLoginButton();

        loginPage.login("PricillaBrundige" + "\n", "wrongPassword");

        loginPage.verifyWrongLogin();
    }

    //@priority(3)
    @Test(description = "Verify Network Error", groups = "Smoke")
    public void NetworkErrorLogin()
    {
        //welcomePage.disableWifi();

        welcomePage.tapLoginButton();

        //loginPage.enableWifi();

        welcomePage.verifyNetworkError();

        welcomePage.retryLogin();

        loginPage.verifyLoginPage();

        //welcomePage.tapLoginButton();

    }

    @Priority(2)
    @Test(description = "Verify Login flow")
    public void SuccessLogin()
    {
        welcomePage.tapLoginButton();

        loginPage.login("PricillaBrundige " + "\n", "wkSTq2C3");

        homePage.waitForMenuPresent();
    }

}
