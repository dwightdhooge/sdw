package scenarios;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import pages.HomePage;
import setup.Setup;

@Listeners(Setup.class)
public class Menu
{
    private HomePage homepage;

    @BeforeMethod(alwaysRun = true)
    public void setup()
    {
        homepage = new HomePage();

    }


}
