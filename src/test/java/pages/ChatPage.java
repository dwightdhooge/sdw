package pages;

import org.openqa.selenium.By;
import setup.MobileBasePage;

import static utils.ByUtils.id;

public class ChatPage extends MobileBasePage {

    By logo = id("imageview_sdworx_logo", "");
    By chatField = id("edittext_chat_input", "");
    By sendChatButton = id("button_chat_send", "");
    By closeCHatButton = id("button_close_chat", "");

    By chatInput = id("textview_chat_simple_text", "");

    By loadingView = id("loading_view", "");
    By profileIcon = id("imageview_profile", "");
    By userInput = id("textview_chat_user_input", "");
    By chatTime = id("textview_chat_time", "");
    By loadingIndicator = id("loading_view","");


    public void openChat() {
        clickOnElement(chatField);
    }

    public void closeChat() {
        clickOnElement(closeCHatButton);
    }

    public void sendChatMessage(final String chatMessage) {
        enterText(chatField, chatMessage);
        clickOnElement(sendChatButton);
    }

    public void checkLoadingIndicator() {
        testElementPresent(loadingIndicator);
    }

    /*public void waitLoadingIndicator() {
        waitForElementRemoved(loadingIndicator);
    }*/
    public void verifyMessageFromChat(final String welcomeMessage) {
        verifyPropertyElementInList(chatInput, welcomeMessage, "text");
    }

    public void verifyMessageFromUser(final String chatMessage) {
        verifyPropertyElementInList(userInput, chatMessage, "text");
    }

    public void sendChatAndCheck(final String chatMessage) {
        sendChatMessage(chatMessage);
        //waitLoadingIndicator();
        verifyMessageFromUser(chatMessage);
        closeChat();

    }

}
