package pages.absencePages;

import org.openqa.selenium.By;
import setup.MobileBasePage;

import java.util.Arrays;

import static utils.ByUtils.id;
import static utils.ByUtils.xPath;


public class AbsenceRequestPage extends MobileBasePage {

    //Navigation Bar
    By upButton = id("", "");
    By requestButton = id("request_absence", "");

    // HolidayTypeFilter
    By holidayTypeLogo = id("imageview_holiday_type", "");
    By holidayTypeFilter = id("spinner_holiday_types", "");
    By holidayType = xPath("//android.widget.ListView/android.widget.TextView[1]", "");

    // Segmented Controller
    By segmentedControl = id("segmentedcontrol_days", "");
    By buttonOneDay = id("button_one_day", "");
    By buttonMultipleDays = id("button_multiple_days", "");

    // Date Fields
    By startDateField = id("textview_start_date", "");
    By halfDayFilterForStartDate = id("spinner_half_days", "");

    By endDateField = id("textview_end_date", "");
    By halfDayFIlterForEndDate = id("spinner_end_date_dropdown", "");

    By wholeday = xPath("//android.widget.ListView/android.widget.TextView[1]", "");
    By listitem2 = xPath("//android.widget.ListView/android.widget.TextView[2]", "");
    By listitem3 = xPath("//android.widget.ListView/android.widget.TextView[3]", "");


    // Calendar
    By confirmDateButton = id("button1", "");
    By cancelDateButton = id("button2", "");
    By datePicker = id("datePicker", "");
    By selectedDate = id("date_picker_header_date", "");
    By previousMonth = id("prev", "");
    By nextMonth = id("next", "");

    // Comment Fields
    By commmentField = id("edittext_requestleave_reason", "");
    By commentLogo = id("imageview_reason", "");

    public void selectHolidayType() {
        clickOnElement(holidayTypeFilter);
        clickOnElement(holidayType);
    }

    public void selectOneDay() {
        clickOnElement(buttonOneDay);
    }

    public void selectMultipleDays() {
        clickOnElement(buttonMultipleDays);
    }

    public void selectStartDate(String strDay, String strMonth, Boolean inThePast) {
        clickOnElement(this.startDateField);
        verifyDatePicker();
        pickCorrectDate(strDay, strMonth, inThePast);

        clickOnElement(this.confirmDateButton);
    }

    public void selectEndDate(String strDay, String strMonth, Boolean inThePast) {
        clickOnElement(this.endDateField);
        verifyDatePicker();
        pickCorrectDate(strDay, strMonth, inThePast);
        clickOnElement(this.confirmDateButton);
    }

    public void selectWholeDay() {
        clickOnElement(wholeday);
    }

    public void selectListItem2() {
        clickOnElement(listitem2);
    }

    public void selectListItem3() {
        clickOnElement(listitem3);
    }

    public void selectSingleDay_Forenoon() {
        clickOnElement(halfDayFilterForStartDate);
        selectListItem2();
    }

    public void selectSingleDay_Afternoon() {
        clickOnElement(halfDayFilterForStartDate);
        selectListItem3();
    }

    public void selectMultipleDay_StartDateAfternoon() {
        clickOnElement(halfDayFilterForStartDate);
        selectListItem2();
    }

    public void selectMultipleDay_EndDateForenoon() {
        clickOnElement(halfDayFIlterForEndDate);
        selectListItem2();
    }

    public void confirmAbsenceRequest() {
        clickOnElement(this.requestButton);
    }

    public void verifyDatePicker() {
        isElementPresent(datePicker);
    }

    public void pickCorrectDate(String strDay, String strMonth, Boolean inThePast) {
        By date = By.xpath("//android.view.View[contains(@text, '" + strDay + "')]");
        while (!isMonthCorrect(strMonth)) {

            if (inThePast == true) {
                clickOnElement(previousMonth);
            } else {
                clickOnElement(nextMonth);
            }
            clickOnElement(date);
            verifyPropertyElement(date, "checked", "true");
        }
    }

    private boolean isMonthCorrect(final String strMonth) {
        final String date = getPropertyElement(selectedDate, "text");

        String[] parts = date.split(" ");

        boolean result = Arrays.stream(parts).anyMatch(strMonth::equals);

        // boolean result = Arrays.stream(parts).anyMatch("Jan"::equals);

        return result;
    }

    public void enterComment(final String strComment) {

        enterText(commmentField, strComment);
    }
}
