package pages.absencePages;

import org.openqa.selenium.By;
import org.testng.Assert;
import setup.MobileBasePage;

import static utils.ByUtils.id;
import static utils.ByUtils.xPath;

public class AbsenceOverviewPage extends MobileBasePage {


    //Navigation Bar
    By upButton = id("", "");
    By addRequestButton = id("Add an absence", "");

    //Remaining Days
    By remainingLabel = id("textview_days_remaining_label", "");
    By remainingDays = id("textview_days_remaining", "");
    By totalDays = id("textview_days_remaining_total", "");

    //TabBar
    By allTab = xPath("//android.widget.TextView[contains(@text, 'ALL')]", "");
    By pendingTab = xPath("//android.widget.HorizontalScrollView/*/*/android.widget.TextView[contains(@text, 'PENDING')]", "");
    By approvedTab = xPath("//android.widget.HorizontalScrollView/*/*/android.widget.TextView[contains(@text, 'APPROVED')]", "");
    By rejectedTab = xPath("//android.widget.HorizontalScrollView/*/*/android.widget.TextView[contains(@text, 'REJECTED')]", "");
    By pastTab = xPath("//android.widget.TextView[contains(@text, 'PAST')]", "");

    //AbsenceList
    By absenceDate = id("textview_absence_date", "");
    By absenceDescription = id("textview_absence_description", "");
    By absenceState = id("textview_absence_state", "");


    public void addAbsenceRequest() {
        clickOnElement(addRequestButton);
    }

    public void waitForRemaingDays() {
        waitForElementPresent(remainingDays);
    }

    public void verifyRemainingDays() {
        testElementPresent(remainingLabel);
        testElementPresent(remainingDays);
        testElementPresent(totalDays);
    }

    public double getRemainingDays() {
        String days = getPropertyElement(remainingDays, "text");
        final double dblDays = Double.parseDouble(days);
        return dblDays;
    }

    public void navigateToAllTab() {
        clickOnElement(allTab);
    }

    public void navigateToPendingTab() {
        clickOnElement(pendingTab);
    }

    public void navigateToApprovedTab() {
        clickOnElement(approvedTab);
    }

    public void navigateToRejectedTab() {
        clickOnElement(rejectedTab);
    }

    public void navigateToPastTab() {
        clickOnElement(pastTab);
    }

    public void checkDateDifference(double startDays, double endDays, double expected) {
        final Double difference = startDays - endDays;
        System.out.println("Testje" + difference);
        Assert.assertEquals(difference, expected);
    }
}
