package pages.absencePages;

import org.openqa.selenium.By;
import setup.MobileBasePage;

import static utils.ByUtils.id;

public class ConfirmationRequestPage extends MobileBasePage {
    By confirmButton = id("request_absence", "");
    By overlay = id("view_overlay"," ");

    public void confirmAbsence(){
        clickOnElement(confirmButton);
    }
}
