package pages.menuPages;

import org.openqa.selenium.By;
import setup.MobileBasePage;

import static utils.ByUtils.id;


public class MenuPage extends MobileBasePage {
    By closeButton = id("button_close", "");
    By AbsenceItem = id("imageview_home_menu_item", "");


    public void closeMenu() {
        clickOnElement(closeButton);
    }

    public void TapAbsenceItem() {
        clickOnElement(AbsenceItem);
    }
}

