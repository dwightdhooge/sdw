package pages;

import org.openqa.selenium.By;
import setup.MobileBasePage;

import static utils.ByUtils.id;

public class HomePage extends MobileBasePage {

    By menuButton = id("button_menu", "");
    By greeting = id("textview_greeting", "");
    By welcomeMessage = id("textview_welcome_message", "");
    By requestHolidayAction = id("button_quickaction", "Request a holiday");
    By chatFooter = id("view_chat_content_bg","");




    public void openMenu() {
        clickOnElement(menuButton);
    }

    public void waitForMenuPresent() {
        waitForElementPresent(menuButton);
    }

    public void addAbsenceRequest() {
        clickOnElement(requestHolidayAction);
    }


}
