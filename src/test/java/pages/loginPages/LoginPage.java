package pages.loginPages;

import org.openqa.selenium.By;
import setup.MobileBasePage;

import static utils.ByUtils.id;
import static utils.ByUtils.xPath;


public class LoginPage extends MobileBasePage {
    By usernameField = id("Username", "");
    By passwordField = id("Password", "");
    By loginButton = xPath("//android.view.View[4]/android.widget.Button");
    By cancelButton = id("com.android.chrome:id/close_button", "");
    By forgotPwLink = id("Forgot your password?", "");
    By lostMailLink = id("Lost your activation mail?", "");
    By authenthicationError = id("accessdenied", "");


    public void enterUsername(final String strName) {
        enterText(this.usernameField, strName);
        dismissKeyboard();
    }

    public void enterPassword(final String strPassword) {
        clickOnElement(this.passwordField);
        enterText(this.passwordField, strPassword);
        dismissKeyboard();
    }

    public void enterCredentials(final String strName, final String strPassword) {
        enterUsername(strName);
        enterPassword(strPassword);
    }

    public void confirmLogin() {
        clickOnElement(this.loginButton);
    }

    public void login(final String strName, final String strPassword) {
        enterCredentials(strName, strPassword);
        confirmLogin();
    }

    public void cancelLogin() {
        clickOnElement(cancelButton);
    }

    public void verifyWrongLogin() {
        testElementPresent(authenthicationError);
        String errorText = getPropertyElement(authenthicationError, "text");
        System.out.println(errorText);
    }

    public void verifyLoginPage() {
        testElementPresent(usernameField);
        testElementPresent(passwordField);
        testElementPresent(loginButton);
    }


}

