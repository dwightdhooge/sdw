package pages.loginPages;

import org.openqa.selenium.By;
import setup.MobileBasePage;

import static utils.ByUtils.id;


public class WelcomePage extends MobileBasePage {
    By loginButton = id("button_login", "");
    By faqLink = id("textview_goto_faq", "");
    By titleText = id("textview_login_title", "");
    By loginText = id("textview_login_message", "");
    By errorTitle = id("alertTitle", "");
    By errorText = id("message", "");
    By tryAgainButton = id("button1", "");
    By logo = id("imageview_sdworx_logo", "");


    public void tapLoginButton() {
        clickOnElement(loginButton);
    }

    public void openFaq() {
        clickOnElement(faqLink);
    }

    public void verifyFAQ(final String link) {
        verifyOpenedUrl(link);
    }

    public void verifyPage() {
        testElementPresent(titleText);
        testElementPresent(loginText);
    }

    public void verifyNetworkError() {
        testElementPresent(errorTitle);
        String Title = getPropertyElement(errorTitle, "text");
        testElementPresent(errorText);
        String Text = getPropertyElement(errorText, "text");
    }

    public void retryLogin() {

        while (isElementPresent(tryAgainButton)) {
            clickOnElement(tryAgainButton);
        }

    }
}
