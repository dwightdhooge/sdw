package utils;

import model.Constants;
import model.Platform;
import org.testng.Reporter;

public class PlatformUtils {
    public static Platform getCurrentPlatform() {
        final String platformName = (String) Reporter.getCurrentTestResult().getTestContext().getAttribute(Constants.PLATFORM_NAME);
        return Platform.getPlatformFromName(platformName);
    }

    public static boolean equalsCurrentPlatform(Platform platform) {
        return getCurrentPlatform().equals(platform);
    }


}
