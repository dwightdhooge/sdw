package utils;

import io.appium.java_client.AppiumDriver;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import setup.MobileBasePage;

import javax.annotation.Nonnull;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TestUtils {
    private static final String SEPARATOR = File.separator;

    /**
     * Captures the current screen and returns the path to the stored image file.
     */
    public static String takeScreenshot(@Nonnull final TakesScreenshot driver) {
        final String folderName = "screenshot";
        final DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy__hh_mm_ssaa");
        final File file;

        // If current context is web, it's not possible to take a screenshot: switch to native context first.
        if (driver instanceof AppiumDriver && !((AppiumDriver) driver).getContext().equals(MobileBasePage.NATIVE_CONTEXT)) {
            final String originalContext = ((AppiumDriver) driver).getContext();
            ((AppiumDriver) driver).context(MobileBasePage.NATIVE_CONTEXT);
            file = driver.getScreenshotAs(OutputType.FILE);
            ((AppiumDriver) driver).context(originalContext);
        } else {
            file = driver.getScreenshotAs(OutputType.FILE);
        }

        // Create dir with given folder name and set filename
        new File(folderName).mkdir();
        final String fileName = dateFormat.format(new Date()) + ".png";

        // Copy screenshot file into screenshot folder.
        try {
            FileUtils.copyFile(file, new File(folderName + SEPARATOR + fileName));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return System.getProperty("user.dir") + SEPARATOR + folderName + SEPARATOR + fileName;
    }
}
