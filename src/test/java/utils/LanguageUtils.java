package utils;

import model.Constants;
import model.Language;
import org.testng.Reporter;

public class LanguageUtils {

    //Get current device language and convert to Language Object
    public static Language getCurrentLanguage() {
        final String devicelanguage = (String) Reporter.getCurrentTestResult().getTestContext().getAttribute(Constants.DEVICE_LANGUAGE);

        return Language.getLanguageFromDeviceLanguage(devicelanguage);
    }

    public static boolean equalsCurrentLanguage(Language language) {
        return getCurrentLanguage().equals(language);
    }

}
