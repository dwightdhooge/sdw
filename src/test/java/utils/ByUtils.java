package utils;

import model.IdentifierType;
import model.Platform;
import org.openqa.selenium.By;

public class ByUtils {
    public static By mixed(IdentifierType androidType, String androidIdentifier, IdentifierType iOSType, String iOSIdentifier) {
        if (PlatformUtils.getCurrentPlatform().equals(Platform.ANDROID)) {

            return getCorrectIdentifier(androidType, androidIdentifier);
        } else {
            return getCorrectIdentifier(iOSType, iOSIdentifier);
        }
    }

    private static By getCorrectIdentifier(IdentifierType platformType, String platformId) {
        switch (platformType) {
            case ID:
                return By.id(platformId);
            case PATH:
                return By.xpath(platformId);
            default:
                throw new IllegalArgumentException("No identifierType found" + platformType);
        }
    }
    /**
     * Use this method when the element has an id on both platforms
     */

    public static By id(String androidId, String iOSId) {
        if (PlatformUtils.getCurrentPlatform().equals(Platform.ANDROID)) {
            return getCorrectIdentifier(IdentifierType.ID, androidId);
        } else {
            return getCorrectIdentifier(IdentifierType.ID, iOSId);
        }
    }

    /**
     * Only use this method when the element is only used for one platform
     */
    public static By id(String id) {
        return id(id, id);
    }

    /**
     * Use this method when the element has an xpath on both platforms
     */
    public static By xPath(String androidPath, String iOSPath) {
        if (PlatformUtils.getCurrentPlatform().equals(Platform.ANDROID)) {
            return getCorrectIdentifier(IdentifierType.PATH, androidPath);
        } else {
            return getCorrectIdentifier(IdentifierType.PATH, iOSPath);
        }
    }

    /**
     * Only use this method when the element is only used for one platform
     */
    public static By xPath(String xpath) {
        return xPath(xpath, xpath);
    }

}
